//
//  ImageInfo.m
//  FSCacheOBJC
//
//  Created by Andy Yeh on 2019/1/15.
//  Copyright © 2019年 Andy Yeh. All rights reserved.
//

#import "ImageInfo.h"

@interface ImageInfo()
@property(nonatomic , readwrite , nullable) NSData *sourceData;
@property(nonatomic , readwrite , nonnull) NSDate *timeStamp;
@property(nonatomic , readwrite) uint size;
#define DEFAULT_COMPRESSED_RATIO 0.5;
@end

@implementation ImageInfo

-(id)initWithInfo: (NSData*) sourceData withTimeStamp:(NSDate*) timeStamp bySourceSize:(uint) size withCompressRatio:(double) compressRatio {
    self = [super init];
    if (self) {
        _sourceData = sourceData;
        _timeStamp = timeStamp;
        _size = size;
        _compressRatio = compressRatio;
    }
    return self;
}

-(id)init {
    self = [super init];
    if (self) {
        _sourceData = nil;
        _timeStamp = [NSDate date];
        _size = 0;
        _compressRatio = DEFAULT_COMPRESSED_RATIO;
    }
    return self;
}

-(NSData*)getCompressdData{
    return _sourceData;
}

@end
