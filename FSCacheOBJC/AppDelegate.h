//
//  AppDelegate.h
//  FSCacheOBJC
//
//  Created by Andy Yeh on 2019/1/14.
//  Copyright © 2019年 Andy Yeh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

