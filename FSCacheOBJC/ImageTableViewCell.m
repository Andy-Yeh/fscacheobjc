//
//  ImageTableViewCell.m
//  FSCacheOBJC
//
//  Created by Andy Yeh on 2019/1/22.
//  Copyright © 2019年 Andy Yeh. All rights reserved.
//

#import "ImageTableViewCell.h"

@interface ImageTableViewCell()
@property(strong ,nonatomic , readwrite , nullable) UIImageView* cellImageContentView;
@property(strong ,nonatomic , readwrite) UIActivityIndicatorView *spinner;
@property(nonatomic , readwrite , nonnull) NSURL *bindingUrl;
@end

@implementation ImageTableViewCell

-(void)bindData: (NSURL*)url withFSCache: (FSCacheManager*) cache {
    
    _bindingUrl = [url copy];
    
    void (^cacheingSuccessBlock)(NSURL* ,NSData* ) = ^(NSURL *url, NSData *data){
        UIImage* image = [UIImage imageWithData: data];
        if (self->_cellImageContentView.image == nil) {
            if ([self->_bindingUrl isEqual: url]) {
                self->_cellImageContentView.image = image;
                [self->_spinner stopAnimating];
                NSLog(@"cacheing successed block did executed");
            }
        }
    };
    [_spinner setHidden: NO];
    [_spinner startAnimating];
    [cache cacheingData: url sucess: cacheingSuccessBlock];
    
}

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle: style reuseIdentifier: reuseIdentifier];
    if (self) {
        [self customInit];
    }
    return self;
}

-(void)customInit {
    _cellImageContentView = [[UIImageView alloc]initWithFrame: CGRectZero];
    _cellImageContentView.image = nil;
    _cellImageContentView.backgroundColor = UIColor.lightGrayColor;
    [self addSubview: _cellImageContentView];
    _spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle: UIActivityIndicatorViewStyleWhiteLarge];
    [self setSelectionStyle: UITableViewCellSelectionStyleNone];
    
    [self addSubview: _spinner];
    [self putAnchors];
}

-(void)resetUIObject {
    _cellImageContentView.image = nil;
    [_spinner setHidden: YES];
}

-(void)putAnchors {
    _cellImageContentView.translatesAutoresizingMaskIntoConstraints = NO;
    [_cellImageContentView.heightAnchor constraintEqualToAnchor: self.heightAnchor multiplier: 0.9].active = YES;
    [_cellImageContentView.widthAnchor constraintEqualToAnchor: _cellImageContentView.heightAnchor multiplier: 0.75].active = YES;
    [_cellImageContentView.leadingAnchor constraintEqualToAnchor: self.leadingAnchor constant: 10.0].active = YES;
    [_cellImageContentView.centerYAnchor constraintEqualToAnchor: self.centerYAnchor constant: 0.0].active = YES;
    _spinner.translatesAutoresizingMaskIntoConstraints = NO;
    [_spinner.centerXAnchor constraintEqualToAnchor: _cellImageContentView.centerXAnchor].active = YES;
    [_spinner.centerYAnchor constraintEqualToAnchor: _cellImageContentView.centerYAnchor].active = YES;
}

-(void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
