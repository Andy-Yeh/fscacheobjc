//
//  RootDirInspector.h
//  FSCacheOBJC
//
//  Created by Andy Yeh on 2019/1/17.
//  Copyright © 2019年 Andy Yeh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FileAttributes.h"

NS_ASSUME_NONNULL_BEGIN
@interface RootDirInspector : NSObject
@property(nonatomic , nonnull ,readonly) NSString* rootDirPath;
-(void)inspectFileFromUrl: (NSURL*)url withFileAttributes: (FileAttributes *) fileAttributes;
-(void)removeFileInspectByPath: (NSString*)path;
-(id)getFileAttributesByUrl: (NSURL*) url;
-(NSString*)getfilePathByUrl: (NSURL*) url;
-(uint)getfileSizeByUrl: (NSURL*) url;
-(NSDate*)getfileCreationTimeByUrl: (NSURL*) url;
-(uint)fileCount;
-(BOOL)isFileExistdFromUrl: (NSURL*) url;
-(id)initWithRootDirPath: (NSString*) path;
@end

NS_ASSUME_NONNULL_END
