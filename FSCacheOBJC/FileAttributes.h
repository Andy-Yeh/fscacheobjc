//
//  FileAttributes.h
//  FSCacheOBJC
//
//  Created by Andy Yeh on 2019/1/17.
//  Copyright © 2019年 Andy Yeh. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN


//用來記錄檔案系統的屬性
@interface FileAttributes : NSObject
//加入Disk Cache的時間
@property(nonatomic , nonnull , readonly) NSDate* creationTime;
//資料大小
@property(nonatomic , readonly) uint fileSize;
-(id)initWithCreationTime: (NSDate*) creationTime andFileSize: (uint) fileSize;
@end

NS_ASSUME_NONNULL_END
