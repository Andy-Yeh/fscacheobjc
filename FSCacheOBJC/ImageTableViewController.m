//
//  ImageTableViewController.m
//  FSCacheOBJC
//
//  Created by Andy Yeh on 2019/1/22.
//  Copyright © 2019年 Andy Yeh. All rights reserved.
//

#import "ImageTableViewController.h"
#import "ImageInfo.h"
#import "FSMemoryCache.h"
#import "FSCacheManager.h"
#import "ImageTableViewCell.h"


@interface ImageTableViewController ()
@property ( nonatomic , readwrite) NSMutableArray<NSURL*> *urlsArray;
@property ( nonatomic , nonnull , readwrite) FSCacheManager *cacheManager;
@end

@implementation ImageTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"imgur";
    self.navigationController.navigationBar.prefersLargeTitles = YES;
    self.navigationController.navigationBar.backgroundColor = UIColor.groupTableViewBackgroundColor;
    [[self tableView] registerClass:ImageTableViewCell.class forCellReuseIdentifier: @"imageTableViewCell"];
    _cacheManager = [[FSCacheManager alloc] initWithMemoryCacheSize: 10 byDiskCacheSize: 20];
    
    NSURL *url1 = [NSURL URLWithString: @"https://i.imgur.com/BJrZ27h.jpg"];
    NSURL *url2 = [NSURL URLWithString: @"https://i.imgur.com/a1Mt4gj.jpg"];
    NSURL *url3 = [NSURL URLWithString: @"https://i.imgur.com/trLWdvE.jpg"];
    NSURL *url4 = [NSURL URLWithString: @"https://i.imgur.com/WxxMkBh.png"];
    NSURL *url5 = [NSURL URLWithString: @"https://i.imgur.com/ui5PwAG.jpg"];
    NSURL *url6 = [NSURL URLWithString: @"https://i.imgur.com/PZtGLnS.jpg"];
    NSURL *url7 = [NSURL URLWithString: @"https://i.imgur.com/qUaBhGn.jpg"];
    NSURL *url8 = [NSURL URLWithString: @"https://i.imgur.com/e1KGQu7.jpg"];
    NSURL *url9 = [NSURL URLWithString: @"https://i.imgur.com/DYWZK2t.jpg"];
    NSURL *url10 = [NSURL URLWithString: @"https://i.imgur.com/nZhxd73.jpg"];
    NSURL *url11 = [NSURL URLWithString: @"https://i.imgur.com/zCXTDQZ.png"];
    NSURL *url12 = [NSURL URLWithString: @"https://i.imgur.com/Ic9k1Vk.jpg"];
    NSURL *url13 = [NSURL URLWithString: @"https://i.imgur.com/OgHGC5C.jpg"];
    NSURL *url14 = [NSURL URLWithString: @"https://i.imgur.com/6oBRcsD.jpg"];
    NSURL *url15 = [NSURL URLWithString: @"https://i.imgur.com/18CyRWB.jpg"];
    
    _urlsArray = [[NSMutableArray alloc] init];
    [_urlsArray addObject: url14];
    [_urlsArray addObject: url14];
    [_urlsArray addObject: url14];
    [_urlsArray addObject: url14];
    [_urlsArray addObject: url14];
    [_urlsArray addObject: url14];
    [_urlsArray addObject: url14];
    [_urlsArray addObject: url14];
    [_urlsArray addObject: url14];
    [_urlsArray addObject: url14];
    [_urlsArray addObject: url1];
    [_urlsArray addObject: url2];
    [_urlsArray addObject: url3];
    [_urlsArray addObject: url4];
    [_urlsArray addObject: url5];
    [_urlsArray addObject: url6];
    [_urlsArray addObject: url7];
    [_urlsArray addObject: url8];
    [_urlsArray addObject: url9];
    [_urlsArray addObject: url10];
    [_urlsArray addObject: url11];
    [_urlsArray addObject: url12];
    [_urlsArray addObject: url13];
    [_urlsArray addObject: url14];
    [_urlsArray addObject: url15];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 150;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _urlsArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ImageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"imageTableViewCell" forIndexPath: indexPath];
    [cell resetUIObject];
    return cell;
}


-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    NSURL *url = _urlsArray[indexPath.row];
    [(ImageTableViewCell*)cell bindData: url withFSCache: _cacheManager];
}

@end
