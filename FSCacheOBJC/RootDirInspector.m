//
//  RootDirInspector.m
//  FSCacheOBJC
//
//  Created by Andy Yeh on 2019/1/17.
//  Copyright © 2019年 Andy Yeh. All rights reserved.
//

#import "RootDirInspector.h"
#import "FileAttributes.h"

@interface RootDirInspector()
@property(nonatomic, readwrite , nonnull)NSMutableDictionary* filesInfoDictionary;
@property(nonatomic , readonly , nonnull)NSFileManager* fileManager;
@end

@implementation RootDirInspector


-(id)initWithRootDirPath: (NSString*) path{
    self = [super init];
    if (self) {
        _rootDirPath = [NSString stringWithString: path];
        _fileManager = NSFileManager.defaultManager;
        _filesInfoDictionary = NSMutableDictionary.new;
        
        if ([_fileManager fileExistsAtPath: _rootDirPath]) {
            //inspect every files at _rootDirPath
            NSArray *namesOfFiles = [_fileManager contentsOfDirectoryAtPath:_rootDirPath error: nil];
            for (NSString *fileName  in namesOfFiles){
                NSString *filePath = [_rootDirPath stringByAppendingPathComponent: fileName];
                NSDictionary *fileAttributesInFileSystem = [_fileManager attributesOfItemAtPath: filePath error: nil];
                NSDate *fileCreationDate = [fileAttributesInFileSystem fileCreationDate];
                unsigned long long int fileSize = [fileAttributesInFileSystem fileSize];
                FileAttributes *attributes = [[FileAttributes alloc] initWithCreationTime: fileCreationDate andFileSize: (uint)fileSize ];
                [_filesInfoDictionary setValue: attributes forKey: filePath];
            }
        }
    }
    return self;
}

-(void)inspectFileFromUrl: (NSURL*)url withFileAttributes: (FileAttributes *) fileAttributes{
    FileAttributes *attributes = fileAttributes;
    NSString *filePath = [self getfilePathByUrl: url];
    [_filesInfoDictionary setValue: attributes forKey: filePath];
}

-(void)removeFileInspectByPath: (NSString*)path{
    [_filesInfoDictionary removeObjectForKey: path];
}


-(id)getFileAttributesByUrl: (NSURL*) url{
    NSString *filePath = [self getfilePathByUrl: url];
    FileAttributes *attributes = [_filesInfoDictionary objectForKey: filePath];
    return attributes;
}

-(NSString*)getfilePathByUrl: (NSURL*) url{
    NSUInteger fileID = [[url absoluteString] hash];
    NSString *fileName = [[NSString stringWithFormat:@"%lu", fileID] stringByAppendingPathExtension: @"jpg"];
    NSString *filePath = [_rootDirPath stringByAppendingPathComponent: fileName];
    return filePath;
}

-(uint)getfileSizeByUrl: (NSURL*) url{
    FileAttributes *attributes = [self getFileAttributesByUrl: url] ;
    return [attributes fileSize];
}

-(NSDate*)getfileCreationTimeByUrl: (NSURL*) url{
    FileAttributes *attributes = [self getFileAttributesByUrl: url] ;
    NSDate *creationTime = [attributes creationTime];
    return [creationTime copy];
}

-(uint)fileCount {
    return (uint)[_filesInfoDictionary count];
}

-(BOOL)isFileExistdFromUrl: (NSURL*) url{
    if ([self getFileAttributesByUrl: url]) {
        return YES;
    }
    return NO;
}

@end
