//
//  FSDiskCache.m
//  FSCacheOBJC
//
//  Created by Andy Yeh on 2019/1/15.
//  Copyright © 2019年 Andy Yeh. All rights reserved.
//

#import "FSDiskCache.h"
#import "RootDirInspector.h"


//保留一份檔案路徑暫存物件可避免加鎖後的嚴重效能拖累，並且不用頻繁讀寫fileio
@interface FSDiskCache()
@property(nonatomic , readwrite , nonnull)RootDirInspector* rootDirInspector;
@property(nonatomic , readonly , nonnull)NSFileManager* fileManager;
#define DEFAULT_CACHE_SIZE 40;
#define DEFAULT_FIFO_REPLACEMENT_RATIO 0.3;
@end

@implementation FSDiskCache

-(id)initWithCacheSize: (uint) cacheSize andNameOfCacheRootDir: (NSString*) nameOfCacheRootDir {
    self = [super init];
    if (self) {
        _cacheSize = cacheSize;
        _cacheRootDirPath = [ NSHomeDirectory() stringByAppendingPathComponent: [NSString stringWithFormat: @"Documents/%@" , nameOfCacheRootDir]];
        [self commonInit];
    }
    return self;
}

-(id)init{
    self = [super init];
    if (self) {
        _cacheSize = DEFAULT_CACHE_SIZE;
        _cacheRootDirPath = [ NSHomeDirectory() stringByAppendingPathComponent: @"Documents/FSCacheOBJC"];
        [self commonInit];
    }
    return self;
}

-(void)commonInit{
    _fifoReplacementRatio = DEFAULT_FIFO_REPLACEMENT_RATIO;
    _rootDirInspector = [[RootDirInspector alloc] initWithRootDirPath: _cacheRootDirPath];
    _fileManager = NSFileManager.defaultManager;
    
    BOOL isDir = YES;
    if (![_fileManager fileExistsAtPath: _cacheRootDirPath isDirectory: &isDir]) {
        [_fileManager createDirectoryAtPath: _cacheRootDirPath withIntermediateDirectories: YES attributes: nil error: nil];
    }
}


-(void)pushIntoCache: (NSURL*) url withData: (ImageInfo*) data{
    NSString *filePath = [self getFilePathByUrl: url];
    
    @synchronized (self) {
        if( ![self isElementExisted: url]){
            if ([self capacity] <= 0) {
                [self cacheMakeRoom];
            }
            FileAttributes *attributes = [[FileAttributes alloc] initWithCreationTime: [data timeStamp] andFileSize: [data size]];
            [_rootDirInspector inspectFileFromUrl: url withFileAttributes: attributes];
            [_fileManager createFileAtPath: filePath contents: [data sourceData] attributes: nil];
        }
    }
}


-(NSString *)getFilePathByUrl : (NSURL*) url{
    NSUInteger fileID = [[url absoluteString] hash];
    NSString *fileName = [[NSString stringWithFormat:@"%lu", fileID] stringByAppendingPathExtension: @"jpg"];
    NSString *filePath = [_cacheRootDirPath stringByAppendingPathComponent: fileName];
    return filePath;
}

-(NSData*)retriveCacheData: (NSURL*) url{
    NSString *filePath = [self getFilePathByUrl: url] ;
    NSData *data = [NSData dataWithContentsOfFile: filePath];
    return data;
}

-(BOOL)isElementExisted: (NSURL *) url{
    return [_rootDirInspector isFileExistdFromUrl: url];
}

-(uint)capacity{
    return _cacheSize - [self numberOfElementsInCache];
}

-(uint)numberOfElementsInCache{
    return [_rootDirInspector fileCount];
}


-(void) cacheMakeRoom{
    //getRemovingPaths() 必須返回絕對路徑
    NSArray<NSString*> *removingPaths = [self getRemovingPaths];
    for(NSString *path in removingPaths){
        [_rootDirInspector removeFileInspectByPath: path];
        [_fileManager removeItemAtPath: path error: nil];
    }
    
}

-(NSArray<NSString*>*) getRemovingPaths{
    NSArray *items = [_fileManager contentsOfDirectoryAtPath: _cacheRootDirPath error: nil];
    NSMutableDictionary<NSString*, NSDate*>* filesInfoDictionary = NSMutableDictionary.new;
    for (NSString *item in items) {
        NSString *filePath = [_cacheRootDirPath stringByAppendingPathComponent: item];
        NSDictionary *fileAttributes= [_fileManager attributesOfItemAtPath: filePath error: nil];
        NSDate *creationDate = fileAttributes.fileCreationDate;
        [filesInfoDictionary setValue: creationDate forKey: filePath];
    }
    
    NSArray *paths = [filesInfoDictionary allKeys];
    NSArray *sortedPathsByCachedDate = [paths sortedArrayUsingComparator: ^NSComparisonResult(id a , id b){
        NSDate *first = [filesInfoDictionary objectForKey: a];
        NSDate *second = [filesInfoDictionary objectForKey: b];
        return [first compare: second];
    }];
    uint numberOfRemovingPaths = ceil(_cacheSize * _fifoReplacementRatio);
    NSRange removingPathsRange = NSMakeRange(0 , (int)numberOfRemovingPaths );
    return [sortedPathsByCachedDate subarrayWithRange: removingPathsRange];
}


@end
