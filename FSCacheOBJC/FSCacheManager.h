//
//  FSCacheManager.h
//  FSCacheOBJC
//
//  Created by Andy Yeh on 2019/1/17.
//  Copyright © 2019年 Andy Yeh. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FSCacheManager : NSObject
-(id)initWithMemoryCacheSize: (uint) memoryCacheSize byDiskCacheSize: (uint) diskCacheSize;
-(void)setMemoryCacheSize: (uint) newSize;
-(void)setDiskCacheSize: (uint) newSize;
//使用URL去緩存資料
-(void)cacheingData: (NSURL*)url sucess: ( void(^)(NSURL * url ,NSData *data ) ) successHandler;
-(NSData *)retriveCacheData: (NSURL *) url;
-(NSData *)retriveMemoryCacheData: (NSURL *) url;
-(NSData *)retriveDiskCacheData: (NSURL *) url;
@end

NS_ASSUME_NONNULL_END
