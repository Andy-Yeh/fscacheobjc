//
//  FileAttributes.m
//  FSCacheOBJC
//
//  Created by Andy Yeh on 2019/1/17.
//  Copyright © 2019年 Andy Yeh. All rights reserved.
//

#import "FileAttributes.h"

@interface FileAttributes()
@property(nonatomic , nonnull , readwrite) NSDate* creationTime;
@property(nonatomic , readwrite) uint fileSize;
@end

@implementation FileAttributes

-(id)initWithCreationTime: (NSDate*) creationTime andFileSize: (uint) fileSize{
    self = [super init];
    if (self) {
        _creationTime = creationTime;
        _fileSize = fileSize;
    }
    return self;
}

@end
