//
//  FSCacheManager.m
//  FSCacheOBJC
//
//  Created by Andy Yeh on 2019/1/17.
//  Copyright © 2019年 Andy Yeh. All rights reserved.
//

#import "FSCacheManager.h"
#import "FSMemoryCache.h"
#import "FSDiskCache.h"

@interface FSCacheManager()
@property(nonatomic , nonnull , readwrite) FSMemoryCache* memoryCache;
@property(nonatomic , nonnull , readwrite) FSDiskCache* diskCache;
@property(atomic , readwrite) NSMutableSet* workingUrls;
@end

@implementation FSCacheManager
-(id)initWithMemoryCacheSize: (uint) memoryCacheSize byDiskCacheSize: (uint) diskCacheSize{
    self = [super init];
    if (self) {
        _memoryCache = [[FSMemoryCache alloc] initWithCacheSize: memoryCacheSize];
        _diskCache = [[FSDiskCache alloc] initWithCacheSize: diskCacheSize andNameOfCacheRootDir: @"FSDiskCache"];
        [self commonInit];
    }
    return self;
}

-(id)init {
    self = [super init];
    if (self) {
        _memoryCache = [[FSMemoryCache alloc] init];
        _diskCache = [[FSDiskCache alloc] init];
        [self commonInit];
    }
    return self;
}

-(void)commonInit {
    _workingUrls = NSMutableSet.new;
}


-(void)setMemoryCacheSize: (uint) newSize{
    [_memoryCache setCacheSize: newSize];
}

-(void)setDiskCacheSize: (uint) newSize {
    [_diskCache setCacheSize: newSize];
}


-(void)cacheingData: (NSURL*)url sucess: ( void(^)(NSURL * url ,NSData *data ) ) successHandler{
    NSURLSessionDataTask *dataTask = [[NSURLSession sharedSession] dataTaskWithURL: url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (!error) {
            uint sourceSize = (uint)[data length];
            ImageInfo *imageInfo = [[ImageInfo alloc] initWithInfo:data withTimeStamp: [NSDate date]  bySourceSize: sourceSize withCompressRatio: 0.5 ];
            [self->_memoryCache pushIntoCache: url withData: imageInfo];
            [self->_diskCache pushIntoCache: url withData: imageInfo];
            
            NSLog(@"%@ cacheing done" , [url absoluteString]);
            
            dispatch_async(dispatch_get_main_queue(), ^{
                successHandler(url , data);
            });
        }else{
            NSLog(@"%@", [error localizedDescription]);
        }
    }];
    
    NSData *dataInCache = [self retriveCacheData: url];
    if (dataInCache) {
        NSLog(@"data from URL was cached already");
        successHandler(url , dataInCache);
    }else{
        [_workingUrls addObject: url];
        [dataTask resume];
        NSLog(@"fetching data from URL by http...");
    }
}


-(NSData *)retriveCacheData: (NSURL *) url{
    //chache memory cache data first
    //if nil cache disk cache data
    NSData *dataFromMemoryCache = [self retriveMemoryCacheData: url];
    if (dataFromMemoryCache) {
        return dataFromMemoryCache;
    }
    NSData *dataFromDiskCache = [self retriveDiskCacheData: url];
    if (dataFromDiskCache) {
        return dataFromDiskCache;
    }
    return nil;
}

-(NSData *)retriveMemoryCacheData: (NSURL *) url{
    NSData *dataFromMemoryCache = [_memoryCache retriveCacheData: url] ;
    return dataFromMemoryCache;
}

-(NSData *)retriveDiskCacheData: (NSURL *) url{
    NSData *dataFromDiskCache = [_diskCache retriveCacheData: url] ;
    return dataFromDiskCache;
}


@end
