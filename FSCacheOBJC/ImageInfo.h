//
//  ImageInfo.h
//  FSCacheOBJC
//
//  Created by Andy Yeh on 2019/1/15.
//  Copyright © 2019年 Andy Yeh. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ImageInfo : NSObject

@property(nonatomic , readonly , nullable) NSData *sourceData;
@property(nonatomic , readonly , nonnull) NSDate *timeStamp;
@property(nonatomic , readonly ) uint size;
@property(nonatomic , readwrite ) double compressRatio;

-(id)initWithInfo: (NSData*) sourceData withTimeStamp:(NSDate*) timeStamp bySourceSize:(uint) size withCompressRatio:(double) compressRatio ;
-(NSData*)getCompressdData;
@end

NS_ASSUME_NONNULL_END
