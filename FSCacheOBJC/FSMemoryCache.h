//
//  FSMemoryCache.h
//  FSCacheOBJC
//
//  Created by Andy Yeh on 2019/1/15.
//  Copyright © 2019年 Andy Yeh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ImageInfo.h"

NS_ASSUME_NONNULL_BEGIN

@interface FSMemoryCache : NSObject
@property(nonatomic , readwrite ) uint cacheSize;
//cacheSize should be observed(for clear remained size)
//clear function is required(for given a range to clear / clear all)
@property(nonatomic , readwrite) double fifoReplacementRatio;

-(id)initWithCacheSize: (uint) cacheSize;
-(void)pushIntoCache: (NSURL*) url withData: (ImageInfo*) data;
-(NSData*)retriveCacheData: (NSURL*) url;
-(BOOL)isElementExisted: (NSURL *) url;

@end

NS_ASSUME_NONNULL_END
