//
//  FSMemoryCache.m
//  FSCacheOBJC
//
//  Created by Andy Yeh on 2019/1/15.
//  Copyright © 2019年 Andy Yeh. All rights reserved.
//

#import "FSMemoryCache.h"
#import "ImageInfo.h"

@interface FSMemoryCache()
@property(nonatomic , readwrite , nonnull) NSMutableDictionary *cache;
#define DEFAULT_CACHE_SIZE 20;
#define DEFAULT_FIFO_REPLACEMENT_RATIO 0.3;
@end

@implementation FSMemoryCache

-(id)initWithCacheSize: (uint) cacheSize{
    self = [super init];
    if (self) {
        _cacheSize = cacheSize;
        _cache = [[NSMutableDictionary alloc] init];
        _fifoReplacementRatio = DEFAULT_FIFO_REPLACEMENT_RATIO;
    }
    return self;
}

-(id)init {
    self = [super init];
    if (self) {
        _cacheSize = DEFAULT_CACHE_SIZE;
        _cache = [[NSMutableDictionary alloc] init];
        _fifoReplacementRatio = DEFAULT_FIFO_REPLACEMENT_RATIO;
    }
    return self;
}

-(uint)numberOfElementsInCache{
    return (uint)[_cache count] ;
}

-(uint)capacity{
    return _cacheSize - [self numberOfElementsInCache];
}

-(void)pushIntoCache: (NSURL*) url withData: (ImageInfo*) data{
    @synchronized (self) {
        if (![self isElementExisted: url]) {
            if ([self capacity] <= 0) {
                NSLog(@"memory cache capacity exhausted, executing FIFO replacement policy...");
                [self cacheMakeRoom];
            }
            [_cache setValue: data forKey: [url absoluteString]];
            NSLog(@"%@ memory cacheing done" , [url absoluteString]);
        }
        NSLog(@"%d elements in memory cache now" , [self numberOfElementsInCache]);
    }
}

-(NSData*)retriveCacheData: (NSURL*) url{
    ImageInfo *dataInfo = [_cache objectForKey: [url absoluteString]];
    if (dataInfo) {
        NSData *data = [[NSData alloc] initWithData: dataInfo.sourceData];
        return data;
    }else{
        return nil;
    }
}

-(BOOL)isElementExisted: (NSURL *) url{
    if ( [self retriveCacheData: url] ) {
        return YES;
    }
    return NO;
}

-(void) cacheMakeRoom{
    NSArray *removingUrls = [self getRemovingUrls];
    for (NSString *url in removingUrls) {
        [_cache removeObjectForKey: url];
    }
}

-(NSArray*) getRemovingUrls{
    NSArray *urls = [_cache allKeys];
    NSArray *sortedUrlsByCachedDate = [urls sortedArrayUsingComparator: ^NSComparisonResult(id a , id b){
        NSDate *first = [[self->_cache objectForKey: a] timeStamp];
        NSDate *second = [[self->_cache objectForKey: b] timeStamp];
        return [first compare: second];
    }];
    uint numberOfRemovingUrls = ceil(_cacheSize * _fifoReplacementRatio);
    NSRange removingUrlsRange = NSMakeRange(0 , (int)numberOfRemovingUrls );
    return [sortedUrlsByCachedDate subarrayWithRange: removingUrlsRange];
}


@end
